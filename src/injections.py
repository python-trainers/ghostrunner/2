from functools import partial

from trainerbase.codeinjection import AllocatingCodeInjection, CodeInjection
from trainerbase.memory import AOBScan
from trainerbase.process import pm

from memory import money_address, player_position_address


make_aob = partial(AOBScan, module_name="Ghostrunner2-Win64-Shipping.exe", any_byte_marker="*")


infinite_stamina = AllocatingCodeInjection(
    make_aob("F3 0F 11 4B 38 E8 72 2C FF FF F3 0F 10 35 5A 8A BA 02"),
    f"""
        push rax
        mov eax, 777.0
        movd xmm1, eax
        pop rax

        movss [rbx + 0x38], xmm1

        push rdx
        mov rdx, {pm.base_address + 0x1C9EE70}
        call rdx
        pop rdx

        push rax
        mov rax, {pm.base_address + 0x4854C60}
        movss xmm6, [rax]
        pop rax
    """,
    original_code_length=18,
)

infinite_skill_energy = AllocatingCodeInjection(
    make_aob("0F 57 F6 0F 2F FE 73 03 0F 57 FF F3 0F 11 7B 78"),
    """
        xorps xmm6, xmm6

        push rax
        mov eax, 100.0
        movd xmm7, eax
        pop rax

        movss [rbx + 0x78], xmm7
    """,
    original_code_length=16,
)

infinite_ultimate_energy = AllocatingCodeInjection(
    make_aob("0F 57 C0 F3 0F 5C F7 F3 0F 11 B3 C4 00 00 00"),
    """
        xorps xmm0, xmm0
        subss xmm6, xmm7

        xorps xmm6, xmm6

        movss [rbx + 0xC4], xmm6
    """,
    original_code_length=15,
)

update_money_address = AllocatingCodeInjection(
    make_aob("8B 4E 50 89 08 48 8B CF 8B 46 5C 0F AF 46 58"),
    f"""
        push rax
        mov rax, {money_address.base_address}
        mov [rax], rsi
        pop rax

        mov ecx, [rsi + 0x50]
        mov [rax], ecx
        mov rcx, rdi
        mov eax, [rsi + 0x5C]
        imul eax, [rsi + 0x58]
    """,
    original_code_length=15,
)

update_player_position_address = AllocatingCodeInjection(
    make_aob("00 48 85 C0 74 23 0F 10 88 D0 01 00 00 0F 28 C1 F3 0F 11 4D 17", add=6),
    f"""
        push rbx
        mov rbx, {player_position_address.base_address}
        mov [rbx], rax
        pop rbx

        movups xmm1,[rax + 0x1D0]
        movaps xmm0, xmm1
        movss [rbp + 0x17], xmm1
    """,
    original_code_length=15,
)

god_mode = CodeInjection(make_aob("80 B8 98 00 00 00 00 0F 85 * * * * 80 BB D5 06 00 00 00", add=19), b"\x01")
no_dash_cooldown = CodeInjection(make_aob("0F 82 * * * * 0F 29 7C 24 * E8"), bytes.fromhex("0F 84"))
infinite_nitro = CodeInjection(make_aob("F3 0F 11 80 * * * * EB * 48 8B 8C 24"), "nop\n" * 8)
infinite_race_infinite_health = CodeInjection(make_aob("FF CB 89 9F * * * * E8 * * * * 48 8D 54 24 50"), "nop\nnop")
