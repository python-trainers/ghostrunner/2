from trainerbase.gameobject import GameFloat, GameUnsignedInt

from memory import money_address, player_position_address


player_x = GameFloat(player_position_address)
player_y = GameFloat(player_position_address + 0x4)
player_z = GameFloat(player_position_address + 0x8)
money = GameUnsignedInt(money_address)
