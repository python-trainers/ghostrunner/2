from trainerbase.common.teleport import Teleport, Vector3

from objects import player_x, player_y, player_z


tp = Teleport(
    player_x,
    player_y,
    player_z,
    dash_coefficients=Vector3(750, 750, 750),
    minimal_movement_vector_length=50,
)
