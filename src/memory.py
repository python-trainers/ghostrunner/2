from trainerbase.memory import Address, allocate


player_position_address = Address(allocate(), [0x1D0])
money_address = Address(allocate(), [0x50])
