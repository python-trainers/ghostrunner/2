from dearpygui import dearpygui as dpg
from trainerbase.gui.helpers import add_components, simple_trainerbase_menu
from trainerbase.gui.injections import CodeInjectionUI
from trainerbase.gui.misc import SeparatorUI, TextUI
from trainerbase.gui.objects import GameObjectUI
from trainerbase.gui.speedhack import SpeedHackUI
from trainerbase.gui.teleport import TeleportUI

from injections import (
    god_mode,
    infinite_nitro,
    infinite_race_infinite_health,
    infinite_skill_energy,
    infinite_stamina,
    infinite_ultimate_energy,
    no_dash_cooldown,
)
from objects import money
from teleport import tp


@simple_trainerbase_menu("Ghostrunner 2", 650, 400)
def run_menu():
    with dpg.tab_bar():
        with dpg.tab(label="Main"):
            add_components(
                CodeInjectionUI(god_mode, "God Mode", "F1"),
                CodeInjectionUI(infinite_stamina, "Infinite Stamina", "F2"),
                CodeInjectionUI(infinite_skill_energy, "Infinite Skill Energy", "F3"),
                CodeInjectionUI(infinite_ultimate_energy, "Infinite Ultimate Energy", "F4"),
                CodeInjectionUI(no_dash_cooldown, "No Dash Cooldown", "F5"),
                CodeInjectionUI(infinite_nitro, "Infinite Nitro", "F7"),
                SeparatorUI(),
                TextUI("Open and close the Upgrades menu to resolve the address"),
                GameObjectUI(money, "Money", default_setter_input_value=500_000),
                SeparatorUI(),
                TextUI("Infinite Race"),
                CodeInjectionUI(infinite_race_infinite_health, "Infinite Health", "F8"),
            )
        with dpg.tab(label="Teleport & Speedhack"):
            add_components(
                TextUI("Jump to resolve"),
                TeleportUI(tp),
                SeparatorUI(),
                SpeedHackUI(key="Alt", default_factor_input_value=0.28),
            )
